import React from 'react';
import PropTypes from 'prop-types';
import { List, Button, Tag } from 'antd';

const propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  toggleItem: PropTypes.func.isRequired,
  deleteItem: PropTypes.func.isRequired,
  completed: PropTypes.bool.isRequired,
};

const actionsForItem = (id, completed, toggleItem, deleteItem) => {
  if(completed){
    return [
      <Button type="default" icon="rollback" onClick={() => toggleItem(id)} />      
    ]
  }else{
    return [
      <Button type="default" icon="check" onClick={() => toggleItem(id)} />, 
      <Button type="danger" icon="delete" onClick={() => deleteItem(id)} />
    ]
  }
}

const TodoItem = ({ id, title, toggleItem, deleteItem, completed }) => (
  <List.Item actions={actionsForItem(id, completed, toggleItem, deleteItem)}>
    {completed && <Tag color="green">completed</Tag>}
    {title}
  </List.Item>
)

TodoItem.propTypes = propTypes;


export default TodoItem;