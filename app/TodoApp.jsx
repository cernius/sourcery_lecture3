import React from 'react';
import { render } from 'react-dom';
import TodoItem from './TodoItem';
import { Row, Col, Input, List } from 'antd';

const generateUniqueId = () => Math.random().toString(36).substring(2, 15);

export default class TodoApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      todoItemList: [{
        id: generateUniqueId(),
        title: 'Test task',
        complated: false
      }]
    };
    this.deleteItem = this.deleteItem.bind(this);
    this.inputChanged = this.inputChanged.bind(this);
    this.toggleItem = this.toggleItem.bind(this);
  }

  addTodoItem(e){
    e.preventDefault();
    this.setState({
      todoItemList: [
        ...this.state.todoItemList,
        {
          id: generateUniqueId(),
          title: this.state.inputValue,
          completed: false
        }
      ],
      inputValue: ''
    });
  }

  toggleItem(id){
    let index = this.state.todoItemList.findIndex(item => item.id === id);
    let item = this.state.todoItemList[index];
    let todoItemList = [
      ...this.state.todoItemList.slice(0, index),
      {
        ...item,
        completed: !item.completed
      },
      ...this.state.todoItemList.slice(index + 1)
    ]
    this.setState({todoItemList});
  }

  deleteItem(id){
    let todoItemList = this.state.todoItemList.filter(item => item.id !== id);
    this.setState({todoItemList});
  }

  inputChanged(e){
    this.setState({
      inputValue: e.target.value
    });
  }

  render() {
    const {todoItemList} = this.state;

    return (
      <Row
        type="flex"
        align="middle"
        justify="center"
      >
        <Col span={12}>
          <h1>Todo App</h1>
          <form onSubmit={this.addTodoItem.bind(this)}>
            <Input
              placeholder="What need to be done..."
              onChange={this.inputChanged} 
              value={this.state.inputValue}
            />
          </form>
          <List
            style={{marginTop: 8}}
            bordered
            dataSource={todoItemList}
            renderItem={item => (
              <TodoItem
                key={item.id}
                id={item.id}
                title={item.title}
                completed={item.completed}
                toggleItem={this.toggleItem}
                deleteItem={this.deleteItem}
              />
            )}
          />
        </Col>
      </Row>
    );
  }
}
