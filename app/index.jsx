import React from 'react';
import { render } from 'react-dom';
import TodoApp from './TodoApp.jsx';
import './styles.css';

render(<TodoApp />, document.querySelector('.react-app'));